# DemoShop Automation

### Cuprins


- Prezentare generală
- Configurație
- Dependențe
- Instrucțiuni de utilizare
- Structura proiectului


### Prezentare generală

Proiectul DemoShop Automation este o aplicație de testare automată a unei aplicații web de tip magazin online.
Acest proiect utilizează limbajul de programare Java și framework-urile TestNG, Selenide și Allure pentru a efectua teste automate asupra aplicației.

### Configurație

Proiectul utilizează Maven pentru gestionarea dependențelor și construcție.
Versiunea Java folosită este Java 20.
Pentru a rula testele și a genera rapoarte, este necesară o configurație adecvată a mediului de dezvoltare.

### Dependențe
- TestNG - Framework de testare.
- Selenide - Framework pentru automatizarea testelor cu Selenium.
- Allure - Framework pentru generarea de rapoarte și documentare a testelor.

### Instrucțiuni de Utilizare
1. Clonați sau descărcați proiectul în directorul local.
2. Asigurați-vă că aveți o configurație de dezvoltare adecvată pentru a rula Java și Maven.
3. Rulați testele automate folosind Maven.
4. După rularea testelor, rapoartele pot fi generate cu Allure

### Structura Proiectului
- src/main/java: conține codul sursă al proiectului.
- src/test/java: conține testele automate.
- pom.xml: fișierul de configurare Maven.
- alte fișiere și directoare specifice proiectului.