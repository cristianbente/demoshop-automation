package org.fasttrackit;

import org.fasttrackit.config.TestConfiguration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CartPageTest extends TestConfiguration {
    Page demoShopPage;
    CartPage cartPage;
    Header header;
    Footer footer;

    @BeforeTest
    public void setup() {
        demoShopPage = new Page();
        demoShopPage.openPage();
        cartPage = new CartPage();
        header = new Header();
        footer = new Footer();
    }

    @AfterMethod
    public void cleanUp() {
        footer.resetPage();
        screenshot();
    }

    @Test
    public void whenCartPageIsEmptyGreetingsMessageIsDisplayed() {
        header.clickOnTheCartIcon();
        assertEquals(cartPage.getGreetingsMessage(), "How about adding some products in your cart?",
                "When empty cart page is open greetings message must be displayed");
    }

    @Test
    public void deleteProductFromCartTest() {
        Product p = new Product("1");
        p.addToCart();
        assertEquals(header.getCartCounter(), "1");
        header.clickOnTheCartIcon();
        cartPage.withProduct(p);

        cartPage.getProductsInCart().get(0).increaseAmount();
        assertEquals(header.getCartCounter(), "2");
        assertEquals(cartPage.getProductsInCart().get(0).getCount(), "2");

        cartPage.getProductsInCart().get(0).reduceAmount();
        assertEquals(header.getCartCounter(), "1");
        assertEquals(cartPage.getProductsInCart().get(0).getCount(), "1");

        cartPage.getProductsInCart().get(0).clickOnTheDeleteFromCart();
        assertEquals(cartPage.getGreetingsMessage(), "How about adding some products in your cart?",
                "When empty cart page is open greetings message must be displayed");
    }
}
