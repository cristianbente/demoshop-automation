package org.fasttrackit;

import org.fasttrackit.dataprovider.DataProviderForDemoShop;
import org.fasttrackit.dataprovider.User;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class DataProviderTest {
    @Test(dataProvider = "UserDataProvider", dataProviderClass = DataProviderForDemoShop.class)
    public void testMyDataProvider(User user) {
        assertTrue(user.getGreetingsMessage().contains(user.getUserName()), "Expected Name to be " + user.getGreetingsMessage());
    }
}
