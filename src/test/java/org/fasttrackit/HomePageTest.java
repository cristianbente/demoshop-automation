package org.fasttrackit;

import io.qameta.allure.Attachment;
import org.fasttrackit.config.TestConfiguration;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class HomePageTest extends TestConfiguration {
    Page demoShopPage;

    @BeforeTest
    private void setup() {
        demoShopPage = new Page();
        demoShopPage.openPage();
    }

    @Test
    public void whenOpeningDemoShop_PageTitleIsDemoshop() {
        String title = demoShopPage.getTitle();
        assertEquals(title, "Demo shop", "Expected title to be Demo shop.");
    }

    @Test
    @Attachment
    public void whenOpeningPage_HelloGuestMsgIsDisplayed() {
        Header header = new Header();
        String greetingsMsg = header.getGreetingsMsg();
        assertEquals(greetingsMsg, "Hello guest!", "When opening page, Hello guest! greetings message is displayed.");
    }
}
