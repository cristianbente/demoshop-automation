package org.fasttrackit;

import org.fasttrackit.config.TestConfiguration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class AddProductsToCartTest extends TestConfiguration {
    Page demoShopPage;
    Header header;
    Footer footer;

    @BeforeTest
    private void setup() {
        demoShopPage = new Page();
        demoShopPage.openPage();
        header = new Header();
        footer = new Footer();
    }

    @AfterMethod
    public void cleanUp() {
        footer.resetPage();
        screenshot();
    }

    @Test(description = "When adding a product to Cart, the cart Badge is incremented.",
            testName = "Add Products To Cart", priority = 1, suiteName = "Products in Cart")
    public void whenAddingAProductToCart_CartBadgeIsIncremented() {
        Product p = new Product("1");
        p.addToCart();
        assertEquals(header.getCartCounter(), "1", "When adding a product to cart, cart badge is incremented by 1 ");
    }

    @Test
    public void whenAddingTwoIdenticalProductsToCart_CartBadgeIsIncrementedBy2() {
        Product p = new Product("1");
        p.addToCart();
        p.addToCart();
        assertEquals(header.getCartCounter(), "2", "When adding two identical products to cart, cart badge is incremented by 2");
    }

    @Test
    public void whenAddingTwoDifferentProductToCart_CartBadgeIsIncrementedBy2() {
        Product p1 = new Product("1");
        Product p2 = new Product("2");
        p1.addToCart();
        p2.addToCart();
        assertEquals(header.getCartCounter(), "2", "When adding two different products to cart, cart badge is incremented by 2 ");
    }
}
