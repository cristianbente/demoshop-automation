package org.fasttrackit.config;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.testng.ScreenShooter;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;

public class TestConfiguration {

    public static final String SAMSUNG_G_S23_U = "1544x720";

    public TestConfiguration() {
        Configuration.browser = "Chrome";
//        Configuration.browserSize = SAMSUNG_G_S23_U;
        ScreenShooter.captureSuccessfulTests = true;
        ChromeOptions options = new ChromeOptions();
//        options.addArguments("--start-fullscreen");
//        options.addArguments("--mobileEmulation=deviceName=Nexus 5");
        Configuration.browserCapabilities = options;
    }

    @AfterMethod
    public void tearDown() {
        screenshot();
    }

    @Attachment(type = "image/png")
    public byte[] screenshot() {
        return Selenide.screenshot(OutputType.BYTES);
    }
}
