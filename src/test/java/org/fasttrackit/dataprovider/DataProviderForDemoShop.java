package org.fasttrackit.dataprovider;

import org.testng.annotations.DataProvider;

public class DataProviderForDemoShop {

    @DataProvider(name = "UserDataProvider")
    public Object[] createUserData() {
        return new Object[]{
                new User("dino", "choochoo"),
                new User("turtle", "choochoo"),
                new User("beetle", "choochoo"),
        };
    }

    @DataProvider(name = "InvalidUserDataProvider")
    public Object[] createInvalidUserData() {
        return new Object[]{
                new InvalidUser("locked", "choochoo", "The user has been locked out."),
                new InvalidUser("invalidUser", "choochoo", "Incorrect username or password!"),
                new InvalidUser("locked", "invalidPassword", "Incorrect username or password!"),
                new InvalidUser("dino", "", "Please fill in the password!"),
                new InvalidUser("", "somePassword", "Please fill in the username!")
        };
    }
}
