package org.fasttrackit.dataprovider;

public class InvalidUser extends User {

    private final String greetingsMessage;
    private final String errorValidationMsg;
    public InvalidUser(String userName, String password, String errorValidationMsg) {
        super(userName, password);
        this.errorValidationMsg = errorValidationMsg;
        this.greetingsMessage = "Hello guest!";
    }

    @Override
    public String getGreetingsMessage() {
        return greetingsMessage;
    }

    public String getErrorValidationMsg() {
        return errorValidationMsg;
    }
}
