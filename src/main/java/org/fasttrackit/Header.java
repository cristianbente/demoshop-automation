package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Attachment;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class Header {

    private final SelenideElement cartCounter = $(".shopping_cart_badge");
    private final SelenideElement loginIcon = $(".fa-sign-in-alt");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");

    public String getGreetingsMsg() {
        SelenideElement greetingsMessage = $(".navbar-text span span");
        return greetingsMessage.text();
    }

    public void clickOnTheLoginIcon() {
        loginIcon.click();
    }

    @Attachment
    public void clickOnTheCartIcon() {
        cartIcon.click();
        System.out.println("Click on the " +cartIcon  + " from header");
    }

    public String getCartCounter() {
        return cartCounter.getText();
    }
}
