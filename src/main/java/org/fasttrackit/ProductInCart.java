package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ProductInCart {

    private final Product product;
    private final SelenideElement productManagerSection = $(".col-md-auto");
    private final SelenideElement decrement;
    private final SelenideElement increment;
    private final SelenideElement deleteFromCart = $(".fa-trash");

    public ProductInCart(Product product) {
        this.product = product;
        String productId = this.product.getProductId();
        this.decrement = productManagerSection.$(".fa-minus-circle");
        this.increment = productManagerSection.$(".fa-plus-circle");
    }

    public void clickOnTheDeleteFromCart() {
        deleteFromCart.click();
        System.out.println("Click on the " + deleteFromCart + " icon on " + product.getName());
    }

    public String getCount() {
        return productManagerSection.getText();
    }

    public void increaseAmount() {
        increment.click();
    }

    public void reduceAmount() {
        decrement.click();
    }
}
