package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {
    private final SelenideElement greetingsMessage = $(".text-center");
    private final List<ProductInCart> productsInCart = new ArrayList<>();

    public void withProduct(Product p) {
        ProductInCart pic = new ProductInCart(p);
        productsInCart.add(pic);
    }

    public String getGreetingsMessage() {
        return greetingsMessage.getText();
    }

    public List<ProductInCart> getProductsInCart() {
        return productsInCart;
    }
}