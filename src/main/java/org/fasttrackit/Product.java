package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class Product {
    private final String productId;

    private final SelenideElement link;
    private final SelenideElement parentCard;
    private final String name;
    private final String price;
    private final SelenideElement addToCartButton;

    public Product(String productId) {
        this.productId = productId;
        String productLink = String.format("[href='#/product/%s']", productId);
        this.link = $(productLink);
        this.name = link.getText();
        this.parentCard = link.parent().parent();
        this.price = this.parentCard.$(".card-footer span").getText() ;
        this.addToCartButton = this.parentCard.$(".fa-cart-plus");
    }

    public String getProductId() {
        return productId;
    }

    @Step("Add to Cart")
    public void addToCart() {
        addToCartButton.click();
        System.out.println("Click on the Add to Cart Button");
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }
}