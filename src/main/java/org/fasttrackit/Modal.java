package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class Modal {
    private final SelenideElement username = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement submit = $("[type=submit]");

    private final SelenideElement errorMsg = $(".error");


    @Step("Click on the username field")
    public void clickOnTheUsernameField() {
        username.click();
        System.out.println("Click on the username field");
    }

    @Step("Type in {userName}")
    public void typeInUserName(String userName) {
        username.setValue(userName);
        System.out.println("Type in : " + userName);
    }

    @Step("Click on the password field")
    public void clickOnThePasswordField() {
        password.click();
        System.out.println("Click on the password field");
    }

    @Step("Type in {password}")
    public void typeInPassword(String password) {
        this.password.setValue(password);
        System.out.println("Type in : " + password);
    }

    @Step("Click on the Login button")
    public void clickOnTheLoginButton() {
        submit.click();
        System.out.println("Click on the Login button");
    }

    public String getErrorMsg() {
        return errorMsg.text();
    }
}
