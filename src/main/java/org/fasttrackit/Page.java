package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Attachment;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class Page {
    public static final String HOMEPAGE_URL = "https://fasttrackit-test.netlify.app/#/";

    private final SelenideElement modalDialog = $(".modal-dialog");

    public void openPage() {
        System.out.println("Open " + HOMEPAGE_URL);
        open(HOMEPAGE_URL);
    }

    @Attachment
    public boolean isModalDisplayed() {
        return modalDialog.exists() && modalDialog.isDisplayed();
    }

    @Attachment
    public String getTitle() {
        return Selenide.title();
    }
}
